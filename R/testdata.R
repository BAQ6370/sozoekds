#' Random generated numbers and values for explanatory purpose.
#'
#'
#' @format A data frame,100 observations,  4 variables
#' \describe{
#'\item{\code{age}}{int age in years}
#'\item{\code{spending_habits}}{int spendings per month in euro}
#'\item{\code{income}}{int income per month in euro}
#'\item{\code{gender}}{chr genderidentity: male, female, nonbinary}
#'}

"testdata"
