# sozoekds

Package for the course "Data Science for Socioeconomists" at the department of Socioeconomics at University of Hamburg, Germany.

Includes 5 different data sets that will be used within the course: *airbnbbig* and *airbnbsmall* include data about Airbnb listing from London, *calhouse* includes data about the housing market in California, *examscores* is a fictional dataset about grades and the socioeconomic background of students, *testdata* is a fictional dataset about income and spending habits.

## Installation

Either:\
#install.packages("remotes") \
#library(remotes)\
remotes::install_gitlab("BAQ6370/sozoekds", host="gitlab.rrz.uni-hamburg.de")

or:

#install.packages("devtools")\
#library(devtools)\
devtools::install_git("[https://gitlab.rrz.uni-hamburg.de/baq6370/sozoekds.git](https://gitlab.rrz.uni-hamburg.de/bay1977/sozoeko1.git)")

## Support

If you find a bug please contact: [lisamarie.wegner\@uni-hamburg.de](lisamarie.wegner@uni-hamburg.de)

## Roadmap

Package should be used in teaching Datascience for Socioeconomists at WiSo-Fakultaet, area Socioeconomics at Hamburg University in Winter 2023.

## Authors and acknowledgment

Lisa Wegner

##  License

CC BY-NC-ND

##  Project status

Active
