
## code to prepare `airbnb_small` dataset goes here

library(devtools)
library(roxygen2)

airbnbsmall <- airbnb_data_small

usethis::use_data(airbnbsmall, overwrite = TRUE)

